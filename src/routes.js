import HomePage from "./Pages/HomePage.svelte";
import Movie from "./Pages/Movie.svelte";
import ErrorRoute from "./Pages/ErrorRoute.svelte";

const routes = {
  "/": HomePage,
  "/Movie/:id": Movie,
  "*": ErrorRoute,
};

export default routes;
